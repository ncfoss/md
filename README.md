# Markdown - Parser

## This is free and unencumbered software released into the public domain.
<img src="https://api.visitorbadge.io/api/visitors?path=gitlab.ncfoss.md&countColor=%2337d67a&style=flat-square&labelStyle=upper"><br>

## Features
- Inline code using \`code\`
- Bold, underline, italic, strike
- `||spoiler text||` will be parsed as `<spoiler>spoiler text</spoiler>`

## Will not implement
- Multiline code via \`\`\` notation
- Any other markdown features such as links, and headings
- Both features above are easy, but require changes in logic, **or** another `for` for line parsing

## Usage
See <a href="https://gitlab.com/ncfoss/md/-/blob/main/src/js/bmd.js" target="_blank">bmd.js</a>, or minified: <a href="https://gitlab.com/ncfoss/md/-/blob/main/src/js/bmd.min.js" target="_blank">bmd.min.js</a><br>

```js
// Should parse correctly
console.log(bmdParse(
`
*italic***bold *italic***
\\\`
\`a=c**b\`
a|b
||spoiler||

\\_italic\\_ is _italic_
\\*italic\\* is *italic*
\\*\\*bold\\*\\* is **bold**
\\_\\_underline\\_\\_ is __underline__
\\~~strike\\~~ is ~~strike~~

__***~~italic bold strike underline~~__***

~123**a
\\
`
));
```


## License
Unlicense.
