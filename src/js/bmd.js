/*        License: Unlicense  (No rights reserved!)
 * Supported tags: b,i,u,s
 */
;function bmdParse(aText) {
	// ~~strike~~; **bold**; __underline__; _italic_; *italic*
	var 
		c = {"|":1,"~":1},
		s = {aa:0,"`":0},
		o = {"*":0,"_":0,"__":0,"**":0,"~~":0,"~":0,"||":0,"|":0},
		t = {"`":"code","*":"i","_":"i","__":"u","**":"b","~~":"s","~":"","||":"spoiler","|":""};
	for (var i = 0, rv = "", slen = aText.length; i < slen; i++) {
		var
			cc = aText.charAt(0 + i),
			nc = aText.charAt(1 + i),
			nc2 = aText.charAt(2 + i),
			ecc = "\\" === cc;
		// etc
		if (cc in s && s.aa) {
			s.aa = 0, s[cc]--;
			rv += "</"+t[cc]+">";
		} else if (ecc && nc in s) {
			rv += nc, i++;
		} else if (s.aa) {
			rv += cc;
		} else {
			if (ecc && (nc+nc2) in o) {
				rv += nc;
				i++;
			} else if (ecc && nc in o) {
				rv += nc;
				i++;
			} else if (ecc) {
				rv += "\\";
			} else {
				var pair = (cc+nc);
				if (pair in o && (!o[cc] || cc in c)) {
					i++;
					if (o[pair]) {
						o[pair]--;
						rv += "</"+t[pair]+">";
					} else {
						o[pair]++;
						rv += "<"+t[pair]+">";
					}
				} else if (cc in o) {
					if (o[cc]) {
						o[cc]--;
						if (t[cc])
							rv += "</"+t[cc]+">";
						else rv += cc;
					} else {
						o[cc]++;
						if (t[cc])
							rv += "<"+t[cc]+">";
						else rv += cc;
					}
				} else if (cc in s) {
					s.aa = 1;
					s[cc]++;
					if (t[cc])
						rv += "<"+t[cc]+">";
				} else rv += cc;
			}
		}
	}
	// resolve unresolved
	for (var n in s)
		for (var c = s[n];c--;)
			if (s[n] && t[n])
				rv += "</"+t[n]+">";
	for (var n in o)
		for (var c = o[n];c--;)
			if (t[n].length)
				rv += "</"+t[n]+">";
	// return
	return rv;
};
